#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <WebSocketsServer.h>
#include <Servo.h>
#include "common.h"
#include "Servers.h"
#include "ByteBuffer.h"
#include "utils.h"
#include "NTPClient.h"


#define PIN_SERVO           D1
#define PIN_FEED_RESET_IN   D2
#define PIN_PHOTO_INT_IN    D5

#define SERVO_CENTER        91
#define MAX_FEEDING_CTR     9

static Servers          mServers;
static WebSocketsServer mWebSocket = WebSocketsServer(89);
static u8               mClientNum = 0xff;
static u32              mLastTick = 0;
static u32              mCtr = 0;
static u32              mLastUpdated = 0;
static Servo            mServo;
static u8               mFeedingCtr = MAX_FEEDING_CTR;
static u8               mBtnMask = 0;
static bool             mBoolSetup = false;
static String           mStrResult;
static String           mStrNewIP;
static NTPClient        mNTPClient;
static bool             mBoolScanning = false;
static u32              mLastFeed = 0;

static void toggleLED(u8 ctr, u16 interval)
{
    for (u8 i = 0; i < ctr; i++) {
        digitalWrite(BUILTIN_LED, LOW);
        delay(interval);
        digitalWrite(BUILTIN_LED, HIGH);
        delay(interval);
    }
}

static void sendRemain(u8 remain, u32 time)
{
    char buf[20];

    if (mClientNum == 0xff)
        return;

    memset(buf, 0, sizeof(buf));
    if (remain == 0xff) {
        strcpy(buf, "@1ERR");
    } else {
        sprintf(buf, "@1%d", remain);
    }
    mWebSocket.sendTXT(mClientNum, buf);

    struct _tm  tm;
    mNTPClient.getTime(time, tm);
    //LOG("UTC time:\t%d:%d:%d\n", getHours(mActualTime), getMinutes(mActualTime), getSeconds(mActualTime));
    //LOG("%4d-%2d-%2d, %2d:%02d:%02d\n", tm.year + 1970, tm.month, tm.day, tm.hour, tm.minute, tm.second);
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "@2%4d-%d-%d, %02d:%02d", tm.year + 1970, tm.month, tm.day, tm.hour, tm.minute);
    mWebSocket.sendTXT(mClientNum, buf);
}

static void turn(void)
{
    u32  start;
    u8   ret;

    if (mFeedingCtr == 0) {
        toggleLED(2, 150);
        mFeedingCtr++;
    }

    ret = digitalRead(PIN_PHOTO_INT_IN);
    LOG("Initial Photo Int Pin : %d\n", ret);       // LOW = hole detected, HIGH = wall

    start = millis();
    mServo.write(85);
    for (;;) {
        ret = digitalRead(PIN_PHOTO_INT_IN);
        if (ret == HIGH) {                          // wall detected
            ret = 0;
            LOG("wall detected !!\n");
            break;
        }
        if (millis() - start > 2000) {
            LOG("Abnormal Stop !!\n");
            ret = 0xff;
            break;
        }
        delay(0);
    }

    if (ret != 0xff) {
        for (;;) {
            ret = digitalRead(PIN_PHOTO_INT_IN);
            if (ret == LOW) {                           // hole detected
                mFeedingCtr--;
                LOG("Hole detected !!\n");
                ret = mFeedingCtr;
                break;
            }

            if (millis() - start > 2000) {
                LOG("Abnormal Stop !!\n");
                ret = 0xff;
                break;
            }
            delay(0);
        }
    }
    mServo.write(SERVO_CENTER);

    mLastFeed = mNTPClient.getTime();
    sendRemain(ret, mLastFeed);

    if (ret == 0xff) {
        toggleLED(10, 100);
    } else {
        toggleLED(1, 300);
    }
}

static void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght)
{
    switch (type) {
        case WStype_DISCONNECTED:                               // if the websocket is disconnected
            mClientNum = 0xff;
            LOG("[%u] Disconnected!\n", num);
            break;

        case WStype_CONNECTED: {                                // if a new websocket connection is established
                IPAddress ip = mWebSocket.remoteIP(num);
                LOG("[%u] Connected from %s url: %s\n", num, ip.toString().c_str(), payload);
                mClientNum = num;
                sendRemain(mFeedingCtr, mLastFeed);
            }
            break;

        case WStype_TEXT:                                       // if new text data is received
            if (payload[0] == '#') {
                turn();
            }
            break;
    }
}


/*
*************************************************************************************************************
* Waiting Functions
*************************************************************************************************************
*/
static void showWaitingPage(String comment, u16 after, String nextPage)
{
    char    buf[255];
    size_t  size;
    String  strLine;
    String  strOut;
    File	file;
    ESP8266WebServer *webserver = mServers.getWebServer();

    file = SPIFFS.open("/wait.html", "r");
    if (!file) {
        webserver->send(404, "text/plain", "FileNotFound:/wait.html");
        return;
    }

    for (;;) {
        memset(buf, 0, sizeof(buf));
        size_t size = file.readBytesUntil('\n', (char*)buf, (size_t)sizeof(buf));
        if (size <= 0 && file.available() == 0) {
            break;
        }

        strLine = String((char*)buf) + "\n";
        if (strLine.indexOf("<h2>Waiting</h2>") >= 0) {
            strOut += "<h2>" + comment + "</h2>\n";
            strOut += "<script>\n setTimeout(function(){\n window.location.href = '" + nextPage + "';\n }," + String(after) + ");\n </script>\n";
        } else {
            strOut += strLine;
        }
    }
    file.close();
    webserver->send(200, "text/html", strOut);
}


/*
*************************************************************************************************************
* WiFi setup
*************************************************************************************************************
*/
static bool resultAP(void)
{
    mServers.getWebServer()->send(200, "text/html", mStrResult);
    mStrResult = "";
    return true;
}

static bool saveAP(void)
{
    char    buf[255];
    size_t  size;
    String  strLine;
    File	file;
    ESP8266WebServer *webserver = mServers.getWebServer();
    ESP8266WiFiMulti multi;

    if (!webserver->hasArg("ssid") || !webserver->hasArg("pass")) {
        webserver->send(400, "text/plain", "400: Invalid Request");
        return true;
    }

    if (mBoolScanning)
        return true;

    mBoolScanning = true;
    showWaitingPage("Connecting...", 7, "/resultAP");
    multi.addAP(webserver->arg("ssid").c_str(), webserver->arg("pass").c_str());
    LOG("saveAP:%s/%s => count:%d\n", webserver->arg("ssid").c_str(), webserver->arg("pass").c_str(), multi.count());

    u32 start = millis();
    while (multi.run() != WL_CONNECTED) {
        if (millis() - start > 5000)
            break;
        LOG(".");
        delay(200);
    }
    LOG("\r\n");
    LOG("connect:%d\n", multi.run());

    mStrResult = "";
    if (multi.run() == WL_CONNECTED) {
        file = SPIFFS.open("/wifi_info.html", "r");
        if (!file) {
            webserver->send(404, "text/plain", "FileNotFound:/wifi_info.html");
            return true;
        }

        mStrNewIP = WiFi.localIP().toString();
        for (;;) {
            memset(buf, 0, sizeof(buf));
            size_t size = file.readBytesUntil('\n', (char*)buf, (size_t)sizeof(buf));
            if (size <= 0 && file.available() == 0) {
                break;
            }

            strLine = String((char*)buf);
            if (strLine.indexOf("<td id=\"ssid\">") >= 0) {
                mStrResult += "<td>" + WiFi.SSID() + "</td>";
            } else if (strLine.indexOf("<td id=\"ip\">") >= 0) {
                mStrResult += "<td>" + mStrNewIP + "</td>";
            } else {
                mStrResult += strLine;
            }
        }
        file.close();

        String  data = webserver->arg("ssid") + "\n" + webserver->arg("pass") + "\n";
        file = SPIFFS.open("/config.txt", "w");
        file.write((u8*)data.c_str(), data.length());
        file.close();
    } else {
        file = SPIFFS.open("/wifi_fail.html", "r");
        if (!file) {
            webserver->send(404, "text/plain", "FileNotFound:/wifi_fail.html");
            return true;
        }
        for (;;) {
            memset(buf, 0, sizeof(buf));
            size_t size = file.readBytesUntil('\n', (char*)buf, (size_t)sizeof(buf));
            if (size <= 0 && file.available() == 0) {
                break;
            }

            strLine = String((char*)buf);
            if (strLine.indexOf("<td id=\"ssid\">") >= 0) {
                mStrResult += "<td>" + webserver->arg("ssid") + "</td>";
            } else if (strLine.indexOf("<td id=\"Password\">") >= 0) {
                mStrResult += "<td>" + webserver->arg("pass") + "</td>";
            } else {
                mStrResult += strLine;
            }
        }
        file.close();
    }
    mBoolScanning = false;

    return true;
}

static bool showSetupPage(void)
{
    String  strLine;
    int     n;
    File	file;
    char    buf[255];
    size_t  size;
    ESP8266WebServer *webserver = mServers.getWebServer();

    WiFi.disconnect();
    n = WiFi.scanNetworks();
    strLine  = "";
    file = SPIFFS.open("/wifi_setup.html", "r");
    if (!file) {
        webserver->send(404, "text/plain", "FileNotFound:/wifi_setup.html");
        return true;
    }

    for (;;) {
        memset(buf, 0, sizeof(buf));
        size_t size = file.readBytesUntil('\n', (char*)buf, (size_t)sizeof(buf));
        //LOG("read(%3d):%s\n", size, buf);
        if (size <= 0 && file.available() == 0) {
            break;
        }

        strLine += String((char*)buf) + '\n';
        if (!strcmp((char*)buf, "<!-- AP list table -->")) {
            for (int i = 0; i < n; i++) {
                strLine  += "<tr>\n";
                strLine  += "<td>" + WiFi.SSID(i) + "</td>\n";
                strLine  += "<td>" + String(WiFi.RSSI(i)) + "</td>\n";
                String enc = WiFi.encryptionType(i) == ENC_TYPE_NONE ? " " : "O";
                strLine  += "<td>" + enc + "</td>\n";
                strLine  += "</tr>\n";
            }
        }
    }
    webserver->send(200, "text/html", strLine);
    file.close();

    return true;
}

static bool hookSetupPage(String uri)
{
    if (uri.equals("/setup") || uri.endsWith("/"))
        return showSetupPage();
    else if (uri.equals("/saveAP"))
        return saveAP();
    else if (uri.equals("/resultAP"))
        return resultAP();
    else if (uri.equals("/reboot")) {
        showWaitingPage("Reboot...", 10, "http://" + mStrNewIP);
        delay(3000);
        WiFi.softAPdisconnect();
        ESP.reset();
    }

    return false;
}


/*
*************************************************************************************************************
* Utility Functions
*************************************************************************************************************
*/
static String formatBytes(size_t bytes)
{
    if (bytes < 1024) {
        return String(bytes) + "B";
    } else if (bytes < (1024 * 1024)) {
        return String(bytes / 1024.0) + "KB";
    } else if (bytes < (1024 * 1024 * 1024)) {
        return String(bytes / 1024.0 / 1024.0) + "MB";
    }
}

static void startSPIFFS(void)
{
    SPIFFS.begin();
    LOG("SPIFFS started. Contents:\n");
    {
        Dir dir = SPIFFS.openDir("/");
        while (dir.next()) {
            String fileName = dir.fileName();
            size_t fileSize = dir.fileSize();
            LOG("\tFS File: %s, size: %s\r\n", fileName.c_str(), formatBytes(fileSize).c_str());
        }
        LOG("\n");
    }
}

static void startSetup(void)
{
    mBoolSetup = true;
    mServers.setup(true, STR_SOFTAP_SSID, STR_SOFTAP_PASSWD);
    mServers.hookWebserver(hookSetupPage);
    LOG("Setup started.\n");
}

void setup()
{
    File    file;
    char    ssid[50];
    char    pass[50];

    pinMode(PIN_PHOTO_INT_IN, INPUT);
    pinMode(PIN_FEED_RESET_IN, INPUT_PULLUP);
    pinMode(BUILTIN_LED, OUTPUT);

    Serial.begin(115200);
    delay(10);
    LOG("\n\n");

    startSPIFFS();

    if (!SPIFFS.exists("/config.txt") || digitalRead(PIN_FEED_RESET_IN) == LOW) {
        startSetup();
    } else {
        file = SPIFFS.open("/config.txt", "r");
        memset(ssid, 0, sizeof(ssid));
        size_t size = file.readBytesUntil('\n', (char*)ssid, (size_t)sizeof(ssid));
        if (size > 0) {
            LOG("SSID:%s\n", ssid);
            memset(pass, 0, sizeof(pass));
            size = file.readBytesUntil('\n', (char*)pass, (size_t)sizeof(pass));
            if (size >= 0) {
                LOG("PASS:%s\n", pass);
                mServers.setup(false, ssid, pass);
                mNTPClient.setup();
                mServo.attach(PIN_SERVO);
                mServo.write(SERVO_CENTER);
                  mWebSocket.begin();
                  mWebSocket.onEvent(webSocketEvent);
            } else {
                LOG("password invalid!!\n");
                startSetup();
            }
        } else {
            LOG("no ssid!!\n");
            startSetup();
        }
        LOG("Feeder started.\n");
    }

    toggleLED(3, 300);
}

void loop()
{
    mServers.loop();

    if (!mBoolSetup) {
        mNTPClient.loop();
        mWebSocket.loop();
        if (digitalRead(PIN_FEED_RESET_IN) == LOW) {
            mBtnMask = 1;
        } else if (mBtnMask && digitalRead(PIN_FEED_RESET_IN) == HIGH) {
            mBtnMask = 0;
            LOG("Feeding counter reset !!\n");
            mFeedingCtr = MAX_FEEDING_CTR;
            sendRemain(mFeedingCtr, mLastFeed);
            toggleLED(3, 300);
        }
    }
}

