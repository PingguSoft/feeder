/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/


#ifndef _NTP_CLIENT_H_
#define _NTP_CLIENT_H_

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "Common.h"
#include "utils.h"
#include <stdarg.h>

#define NTP_PACKET_SIZE     48
#define NTP_SERVER_DNS      "time.nist.gov"

#define NTP_INTERVAL_INIT   1000
#define NTP_INTERVAL_NORMAL 60000
#define NTP_TIME_ZONE       (9 * 60 * 60)

struct _tm {
    u8 second;
    u8 minute;
    u8 hour;
    u8 wday;   // day of week, sunday is day 1
    u8 day;
    u8 month;
    u8 year;   // offset from 1970;
};

class NTPClient
{
public:
    NTPClient();
    ~NTPClient();

    void setup(void);
    void loop(void);
    u32  getTime(void)                         { return mActualTime;          }
    void getTime(struct _tm &tm);
    void getTime(u32 timeNTP, struct _tm &tm);

    inline static int getSeconds(u32 UNIXTime) { return UNIXTime % 60;        }
    inline static int getMinutes(u32 UNIXTime) { return UNIXTime / 60 % 60;   }
    inline static int getHours(u32 UNIXTime)   { return UNIXTime / 3600 % 24; }

private:
    void sendNTPpacket(IPAddress& address);
    u32  getNTPTime(void);


    // variables
    WiFiUDP     mUDP;
    u8          mNTPBuffer[NTP_PACKET_SIZE];
    IPAddress   mTimeServerIP;
    u16         mInterval;
    u32         mLastNTPReq;
    u32         mLastNTPRes;
    u32         mLastUnixTime;
    u32         mLastActualTime;
    u32         mActualTime;
};

#endif

