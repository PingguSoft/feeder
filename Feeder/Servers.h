#ifndef _SERVERS_H_
#define _SERVERS_H_
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <WebSocketsServer.h>
#include "common.h"
#include "utils.h"


#define STR_SOFTAP_SSID     "PetFeeder"                      // The name of the Wi-Fi network that will be created
#define STR_SOFTAP_PASSWD   NULL                             // The password required to connect to it, NULL for an open network

#define STR_OTA_NAME        "PetFeeder"                      // A name and a password for the OTA service
#define STR_OTA_PASSWD      "PetFeeder"

#define STR_mDNS_NAME       "PetFeeder"                      // Domain name for the mDNS responder

class Servers
{
public:
    Servers()                               {   mStrDefaultPage = "index.html"; }
    void setup(bool isAP, char *szSSID, char *szPass);
    void loop(void);
    void setDefaultPage(char *szFileName);
    ESP8266WebServer *getWebServer(void)    {   return &mWebServer;             }
    ESP8266WiFiMulti *getWiFiMulti(void)    {   return &mWiFiMulti;             }

    typedef std::function<bool(String)> TWebServerHandler;
    void hookWebserver(TWebServerHandler fn);

private:
    ESP8266WiFiMulti mWiFiMulti;                             // Create an instance of the ESP8266WiFiMulti class, called 'mWiFiMulti'
    ESP8266WebServer mWebServer;                             // create a web mWebServer on port 80
    File             mFileUpload;                            // a File variable to temporarily store the received file
    char             *mStrDefaultPage;
    TWebServerHandler mWebServerHandler;

    void startAP(char *szSSID, char *szPass);
    void startWiFi(char *szSSID, char *szPass);
    void startOTA(void);
    void startWebSocket(void);
    void startMDNS(void);
    void handleNotFound(void);
    void handleFileList(void);
    bool handleFileRead(String path);
    void handleFileUpload();
    void handleFileDelete();
    void handleFileCreate();
    String formatBytes(size_t bytes);
    String getContentType(String filename);
};
#endif