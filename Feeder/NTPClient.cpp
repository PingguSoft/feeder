/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/
#include <Arduino.h>
#include "NTPClient.h"
#include "utils.h"

NTPClient::NTPClient()
{
    mLastNTPReq = 0;
    mLastNTPRes = 0;
    mInterval   = NTP_INTERVAL_INIT;
}

NTPClient::~NTPClient()
{
}

void NTPClient::setup(void)
{
    mUDP.begin(123);
    if (!WiFi.hostByName(NTP_SERVER_DNS, mTimeServerIP)) {
        LOG("DNS lookup failed.!!\n");
    } else {
        LOG("TimeServer:%s => %s\n", NTP_SERVER_DNS, mTimeServerIP.toString().c_str());
        sendNTPpacket(mTimeServerIP);
    }
}

u32 NTPClient::getNTPTime(void)
{
    if (mUDP.parsePacket() == 0) {
        return 0;
    }

    mUDP.read(mNTPBuffer, NTP_PACKET_SIZE);     // read the packet into the buffer

    // Combine the 4 timestamp bytes into one 32-bit number
    u32 NTPTime = (mNTPBuffer[40] << 24) | (mNTPBuffer[41] << 16) | (mNTPBuffer[42] << 8) | mNTPBuffer[43];

    // Convert NTP time to a UNIX timestamp:
    // Unix time starts on Jan 1 1970. That's 2208988800 seconds in NTP time:
    // subtract seventy years:

    return NTPTime - 2208988800UL;
}

void NTPClient::sendNTPpacket(IPAddress& address)
{
    memset(mNTPBuffer, 0, NTP_PACKET_SIZE);

    // Initialize values needed to form NTP request
    mNTPBuffer[0] = 0b11100011;   // LI, Version, Mode

    // send a packet requesting a timestamp:
    mUDP.beginPacket(address, 123);

    // NTP requests are to port 123
    mUDP.write(mNTPBuffer, NTP_PACKET_SIZE);
    mUDP.endPacket();
}

void NTPClient::loop(void)
{
    u32 cur = millis();

    if (cur - mLastNTPReq > mInterval && mTimeServerIP) {
        mLastNTPReq = cur;
        sendNTPpacket(mTimeServerIP);
    }

    u32 time = getNTPTime();
    if (time) {
        mLastUnixTime = time + NTP_TIME_ZONE;
        mLastNTPRes   = cur;
        mInterval     = NTP_INTERVAL_NORMAL;
        LOG("UnixTime:%ld\n", mLastUnixTime);
    }

    mActualTime = mLastUnixTime + (cur - mLastNTPRes) / 1000;
    if (mActualTime != mLastActualTime && mLastUnixTime != 0) {
        mLastActualTime = mActualTime;

        struct _tm  tm;
        getTime(tm);
        //LOG("UTC time:\t%d:%d:%d\n", getHours(mActualTime), getMinutes(mActualTime), getSeconds(mActualTime));
        LOG("%4d-%2d-%2d, %2d:%02d:%02d\n", tm.year + 1970, tm.month, tm.day, tm.hour, tm.minute, tm.second);
    }
}

// leap year calulator expects year argument as years offset from 1970
#define IS_LEAP_YEAR(Y)     ( ((1970 + (Y)) > 0) && !((1970 + (Y)) % 4) && ( ((1970 + (Y)) % 100) || !((1970 + (Y)) % 400) ) )

// API starts months from 1, this array starts from 0
static const u8 TBL_MONTH_DAYS[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

void NTPClient::getTime(u32 timeNTP, struct _tm &tm)
{
// break the given time_t into time components
// this is a more compact version of the C library localtime function
// note that year is offset from 1970 !!!

    u8      year;
    u8      month, monthLength;
    u32     time;
    u32     days;

    time = timeNTP;
    tm.second = time % 60;
    time /= 60; // now it is minutes
    tm.minute = time % 60;
    time /= 60; // now it is hours
    tm.hour = time % 24;
    time /= 24; // now it is days
    tm.wday = ((time + 4) % 7) + 1;  // Sunday is day 1

    year = 0;
    days = 0;
    while((unsigned)(days += (IS_LEAP_YEAR(year) ? 366 : 365)) <= time) {
        year++;
    }
    tm.year = year; // year is offset from 1970

    days -= IS_LEAP_YEAR(year) ? 366 : 365;
    time -= days; // now it is days in this year, starting at 0

    days  = 0;
    month = 0;
    monthLength = 0;
    for (month = 0; month < 12; month++) {
        if (month == 1) { // february
            if (IS_LEAP_YEAR(year)) {
                monthLength = 29;
            } else {
                monthLength = 28;
            }
        } else {
            monthLength = TBL_MONTH_DAYS[month];
        }

        if (time >= monthLength) {
            time -= monthLength;
        } else {
            break;
        }
    }
    tm.month = month + 1;   // jan is month 1
    tm.day   = time  + 1;   // day of month
}

void NTPClient::getTime(struct _tm &tm)
{
    getTime(mActualTime, tm);
}